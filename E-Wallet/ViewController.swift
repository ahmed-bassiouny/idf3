//
//  ViewController.swift
//  E-Wallet
//
//  Created by NTAM on 1/27/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {

    var flag = 0;
    @IBOutlet weak var myImage: UIImageView!
    
    
    
    var video = AVCaptureVideoPreviewLayer()
    
    let output = AVCaptureMetadataOutput()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //scanQrCode()
        RequestAPI.shared.transferMoney(userToken: "5035e777c054d41f12a730f031f06ae5d6660d866d345a750f43e6a61630", toPhone: "1234", amount: "2") { result in
            switch result {
            case let .success(msg):
                break
            case let .failure(error):
                break
            }
        }
        
                
    }
    
    func scanQrCode() {
        
        
        // create session
        let session = AVCaptureSession()

        
        // define caputre device
        
        let caputreDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            let input = try AVCaptureDeviceInput(device: caputreDevice!)
            session.addInput(input)
        }catch {
            
        }
        
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        
        // fill the entire screen
        video.frame = view.layer.bounds
        
        view.layer.addSublayer(video)
        
        // view.bringsunbiew(tofront:square)
        
        session.startRunning()
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                if object.type == AVMetadataObject.ObjectType.qr {
                    print(object.stringValue)
                    output.setMetadataObjectsDelegate(nil, queue: DispatchQueue.main)
                }
            }
        }
    }

    @IBAction func clickButton(_ sender: Any) {
        var qrcodeImage: CIImage!
        flag = flag + 1
        let data = String(flag).data(using: .ascii, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        qrcodeImage = (filter?.outputImage)!
        let scaleX = myImage.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = myImage.frame.size.height / qrcodeImage.extent.size.height
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        myImage.image = UIImage(ciImage: transformedImage)
        
        // dialog
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            alert.dismiss(animated: true, completion: nil)
        }
        
    }
    
}

