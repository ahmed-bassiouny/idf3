//
//  CommunicatorProtocol.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation

protocol CommunicatorProtocol {
    
    func finish(number:String)
}
