//
//  Utils.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    static var dateFormatterFrom : DateFormatter?
    static var dateFormatterTo : DateFormatter?
    static let alert = UIAlertController(title: nil, message: Constant.wait, preferredStyle: .alert)
    
    static func showAlert(vc:UIViewController,title:String,msg:String,clickTxt:String)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: clickTxt, style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func showAlert(vc:UIViewController,title:String,msg:String)  {
        showAlert(vc: vc, title: title, msg: msg, clickTxt: Constant.ok)
    }
    
    static func showAlert(vc:UIViewController,msg:String)  {
        showAlert(vc: vc, title: Constant.error, msg: msg, clickTxt: Constant.ok)
    }
    
    static func showDialog(vc:UIViewController) {
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func dismissDialog() {
        alert.dismiss(animated: true, completion: nil)
    }
    
    static func convertDate(date:String)-> String{
        
        if dateFormatterFrom == nil || dateFormatterTo == nil{
            dateFormatterFrom = DateFormatter()
            dateFormatterFrom?.locale = Locale(identifier: "en_US_POSIX")
            dateFormatterFrom?.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            dateFormatterTo = DateFormatter()
            dateFormatterTo?.locale = Locale.current
            dateFormatterTo?.dateFormat = "dd-MM-yyyy"
        }
        
        
        if let date = dateFormatterFrom?.date(from: date) {
            return dateFormatterTo?.string(from: date) ?? ""
        } else {
            return ""
        }
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
   static func getImage(image:UIImageView,url:String)  {
             image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placehoulder.png"))
    }
    
    // share text
    static func shareText(vc:UIViewController,text:String) {
        
        // set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = vc.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        vc.present(activityViewController, animated: true, completion: nil)
        
    }
    
    // share image
    static func shareImage(vc:UIViewController,image:UIImage) {
        

        // set up activity view controller
        let imageToShare = [image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = vc.view // so that
        
        // present the view controller
        vc.present(activityViewController, animated: true, completion: nil)
    }
    
}
