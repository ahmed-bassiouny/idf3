//
//  Cach.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper

class Cach {
    
    static func saveUser(passcode:Int){
        UserDefaults.standard.set(passcode, forKey: Constant.PASSCODE)
    }
    
    static func saveUser(user:User,passcode:Int){
        UserDefaults.standard.set(passcode, forKey: Constant.PASSCODE)
        saveUser(user:user)
    }
    static func saveUser(user:User){
        UserDefaults.standard.set(Mapper().toJSONString(user), forKey: Constant.USER)
    }
    
    static func getPasscode() -> Int {
        return UserDefaults.standard.integer(forKey: Constant.PASSCODE)
    }
    
    static func getUser() -> User? {
        let userStr = UserDefaults.standard.string(forKey: Constant.USER)
        if userStr != nil {
            return Mapper<User>().map(JSONString: userStr!)
        }
        return nil
    }
}
