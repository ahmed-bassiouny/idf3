//
//  Constant.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation

class Constant {
    
    static let ok = "حسنا"
    static let error = "خطا"
    static let wait = "من فضلك انتظر ..."
    static let invalidPhone =  "رقم الهاتف يجب ان يكون ١١ رقم"
    static let invalidVerificationCode = "كود التفعيل خاطئ"
    static let invalidName = "الاسم مطلوب"
    static let invalidPasscode = "الرقم السرى يجب ان يكون ٤ ارقام فقط"
    static let invalidPasscodeLogin = "الرقم السرى غير صحيح"

    static let invalidConfirmPasscode = "الرقم السرى وتاكيد الرقم السرى غير متطابقان"
    static let passcodeChanged = "تم تعديل الرقم السرى بنجاح"
    static let profileUpdated = "تم تعديل بياناتك بنجاح"

    static let USER = "user"
    static let PASSCODE = "passcode"
    static let TOKEN = "token"
    static let success = "تم"
    static let shareTxtInfo = "يحاول تحويل مبلغ مالى عن طريق تطبيق ادفع. قم بتحميل التطبيق من الرابط التالى واستمتع باسعر وامن طريقة للدفع فى مصر"
    static let appLink = "http://ntamtech.com/"

}
