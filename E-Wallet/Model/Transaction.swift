//
//  Transaction.swift
//  E-Wallet
//
//  Created by NTAM on 1/31/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper


struct Transaction: Mappable {
    
    var name: String = ""
    var phone: String = ""
    var isSender: Bool = false
    var creditAmount: String = ""
    var date: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        name  <- map["name"]
        phone     <- map["phone"]
        isSender  <- map["is_sender"]
        creditAmount  <- map["credit_amount"]
        date <- map["created_at"]
    }
}
