//
//  Offer.swift
//  E-Wallet
//
//  Created by NTAM on 3/6/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper

struct Offer: Mappable {
    
    var name: String = ""
    var description: String = ""
    var logo: String = ""
    var offer: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        name  <- map["name"]
        description     <- map["description"]
        logo  <- map["photo"]
        offer  <- map["discount"]
    }
}
