//
//  Abour.swift
//  E-Wallet
//
//  Created by NTAM on 2/13/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper

class About: Response {
    
    var data:String = ""
   
    
    
    override func mapping(map: Map) {
        status  <- map["status"]
        message <- map["message"]
        data    <- map["data"]
    }
}
