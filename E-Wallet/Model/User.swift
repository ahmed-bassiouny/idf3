//
//  User.swift
//  E-Wallet
//
//  Created by NTAM on 1/30/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper


struct User: Mappable {
    var id = 0
    var name: String = ""
    var phone: String = ""
    var verificationCode: String = ""
    var photo: String = ""
    var creditAmount: String = ""
    var userToken: String = ""
    
    init?(map: Map) {
        
    }
    
   mutating func mapping(map: Map) {
        id     <- map["id"]
        name  <- map["name"]
        phone     <- map["phone"]
        verificationCode  <- map["verification_code"]
        photo     <- map["photo"]
        creditAmount  <- map["credit_amount"]
        userToken     <- map["user_token"]
    }
}
