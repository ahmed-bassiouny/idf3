//
//  ExtensionLabel.swift
//  E-Wallet
//
//  Created by NTAM on 2/5/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import UIKit


extension UILabel {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: "GE SS TWO", size: self.font?.pointSize ?? 0)
    }
    
}
