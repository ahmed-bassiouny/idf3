//
//  ExtensionTextField.swift
//  ChatHotel
//
//  Created by NTAM on 1/20/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
extension UITextField {
    
    @IBInspectable var placeholderColor: UIColor? {
        get {
            let attributes = attributedPlaceholder?.attributes(at: 0, effectiveRange: nil) ?? [:]
            return attributes[NSAttributedString.Key.foregroundColor] as? UIColor ?? .gray
        }
        
        set {
            let color: UIColor = newValue ?? .gray
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: color])
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        get {
            let rightImageView = rightView as? UIImageView
            return rightImageView?.image
        }
        
        set {
            if let newImage = newValue {
                rightViewMode = .always
                if let rightImageView = rightView as? UIImageView {
                    rightImageView.image = newImage
                } else {
                    rightView = UIImageView(image: newImage)
                    rightView!.frame = CGRect(x: 0.0, y: 0.0, width: newImage.size.width + 10.0, height: newImage.size.height)
                    rightView!.contentMode = .center
                }
            } else {
                rightView = nil
                rightViewMode = .never
            }
        }
    }
    
    
    @IBInspectable var leftImage: UIImage? {
        get {
            let leftImageView = rightView as? UIImageView
            return leftImageView?.image
        }
        
        set {
            if let newImage = newValue {
                leftViewMode = .always
                if let leftImageView = leftView as? UIImageView {
                    leftImageView.image = newImage
                } else {
                    leftView = UIImageView(image: newImage)
                    leftView!.frame = CGRect(x: 0.0, y: 0.0, width: newImage.size.width - 0.0, height: newImage.size.height)
                    leftView!.contentMode = .center
                }
            } else {
                leftView = nil
                leftViewMode = .never
            }
        }
    }
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: "GE SS TWO", size: self.font?.pointSize ?? 0)
    }
}
