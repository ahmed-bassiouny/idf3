//
//  BaseResponse.swift
//  E-Wallet
//
//  Created by NTAM on 1/31/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponse<T:Mappable> : Response {
    
    
    var data:T?
    
    override func mapping(map: Map) {
        status  <- map["status"]
        message <- map["message"]
        data    <- map["data"]
    }
}

class Response : Mappable{
    var status = false
    var message = ""
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status  <- map["status"]
        message <- map["message"]
    }
}
