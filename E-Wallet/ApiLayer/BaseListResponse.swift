//
//  BaseListResponse.swift
//  E-Wallet
//
//  Created by NTAM on 1/31/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import ObjectMapper

struct BaseListResponse<T:Mappable> : Mappable{
    
    var status = false
    var message = ""
    var data:[T]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status  <- map["status"]
        message <- map["message"]
        data    <- map["data"]
    }
}
