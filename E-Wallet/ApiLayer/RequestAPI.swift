//
//  RequestAPI.swift
//  E-Wallet
//
//  Created by NTAM on 1/31/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Foundation

class RequestAPI {
    
    static let shared = RequestAPI()
    
    private let BASE_URL = "https://ntam.tech/E-Wallet/api"
    
    private init() {
    }
    
    
    func getUserInfo(phone: String,token: String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/user_register"
        let params = ["phone": "\(phone)","fcm_token":token]
      
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse<User>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    
    }
    
    func getUserName(phone: String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/get_data_using_phone"
        let params = ["phone": "\(phone)"]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse<User>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func updateUser(name: String,photo:String,userToken:String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/update_profile"
        let params = ["name": name,"photo":photo,"user_token": userToken]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse<User>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
    
    func getHistory(userToken: String,completion: @escaping (ApiResult<[Transaction]>) -> Void) {
        let url = "\(BASE_URL)/history"
        let params = ["user_token": userToken]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseListResponse<Transaction>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let transaction = result.data {
                        completion(.success(transaction))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func transferMoney(userToken: String,toPhone:String,amount:String,completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/transfer_credit"
        let params = ["user_token": userToken,"receiver_phone":"\(toPhone)","credit_amount":amount]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<Response>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.message))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
    func getUserObject(userToken:String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/balance"
        let params = ["user_token": userToken]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse<User>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
    func getAbout(completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/about"
        Alamofire.request(url, method: .get)
            .responseObject { (response: DataResponse<About>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.data))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func getOffers(completion: @escaping (ApiResult<[Offer]>) -> Void) {
        let url = "\(BASE_URL)/offers"
        
        Alamofire.request(url, method: .get)
            .responseObject { (response: DataResponse<BaseListResponse<Offer>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let transaction = result.data {
                        completion(.success(transaction))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
    func recharge(userToken: String,code:String,completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/get_credit"
        let params = ["user_token": userToken,"barcode":code]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<Response>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.message))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
}
