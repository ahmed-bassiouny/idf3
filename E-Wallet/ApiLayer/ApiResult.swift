//
//  ApiResult.swift
//  E-Wallet
//
//  Created by NTAM on 1/31/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

enum ApiResult<Value> {
    case success(Value)
    case failure(String)
}

