//
//  MyWalletViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/4/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class MyWalletViewController: UIViewController {
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var refreshIcon: UIImageView!
    @IBOutlet weak var refreshLabel: UILabel!
    @IBOutlet weak var refreshView: UIView!
    
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Cach.getUser(){
            balanceLabel.text = user.creditAmount
            token = user.userToken
        }
        
        refreshView.isUserInteractionEnabled = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserInfo()
    }
    
    
    func getUserInfo() {
        RequestAPI.shared.getUserObject(userToken: token)  { result in
            switch result {
            case let .success(user):
                self.balanceLabel.text = user.creditAmount
                self.showRefreshInfo()
                Cach.saveUser(user: user)
                break
            case .failure(_):
                break
            }
        }
    }
    
    func showRefreshInfo() {
        refreshIcon.isHidden = false
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.hour,.minute], from: date)
        
        let hour = components.hour
        let minute = components.minute
        
        if let h = hour , let m = minute {
            refreshLabel.text = "اخر تحديث \(h):\(m) "
        }
    }
    
}
