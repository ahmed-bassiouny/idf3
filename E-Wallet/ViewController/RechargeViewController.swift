//
//  RechargeViewController.swift
//  E-Wallet
//
//  Created by NTAM on 3/6/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class RechargeViewController: UIViewController,CommunicatorProtocol {
    
    @IBOutlet weak var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.scanQR))
        self.myView.isUserInteractionEnabled = true
        self.myView.addGestureRecognizer(gesture)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ScanQRViewController {
            let vc = segue.destination as? ScanQRViewController
            vc?.communicator = self
        }
    }

    @objc func scanQR() {
        //performSegue(withIdentifier: "rechargeByScan", sender: self)
        let newViewController = ScanQRViewController()
        newViewController.communicator = self
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func finish(number: String) {
        recharge(code: number)
    }

    func recharge(code:String) {
        if let user = Cach.getUser() {
            Utils.showDialog(vc: self)
            RequestAPI.shared.recharge(userToken: user.userToken, code: code)  { result in
                switch result {
                case .success(_):
                    Utils.dismissDialog()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
                        Utils.showAlert(vc: self, title:"نجحت العملية",msg: "تم شحن الرصيد")
                    }
                    break
                case let .failure(error):
                    Utils.dismissDialog()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
                        Utils.showAlert(vc: self, msg:error)
                    }
                    break
                }
            }
        }else {
            Utils.showAlert(vc: self, title:"فشلت العملية",msg: "حدث خطا ما")
        }
    }
    
}
