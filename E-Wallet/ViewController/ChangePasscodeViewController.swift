//
//  ChangePasscodeViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/14/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class ChangePasscodeViewController: UIViewController {
    
    
    @IBOutlet weak var currentPasscode: UITextField!
    @IBOutlet weak var newPasscode: UITextField!
    @IBOutlet weak var confirmPasscode: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentPasscode.keyboardType = .asciiCapableNumberPad
        newPasscode.keyboardType = .asciiCapableNumberPad
        confirmPasscode.keyboardType = .asciiCapableNumberPad

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func changePasscode(_ sender: Any) {
        
        let passcodeCach = Cach.getPasscode()
        if passcodeCach == Int(currentPasscode.text ?? "0") {
            if newPasscode.text?.count != 4 {
                Utils.showAlert(vc:self , msg:Constant.invalidPasscode)
            }else if newPasscode.text == confirmPasscode.text{
                Cach.saveUser(passcode: Int(newPasscode.text ?? "0") ?? 0)
                self.navigationController?.popViewController(animated: true)
            }else {
                Utils.showAlert(vc:self , msg:Constant.invalidConfirmPasscode)
            }
        }else {
            Utils.showAlert(vc:self , msg:Constant.invalidPasscodeLogin)
        }
        
    }
    
}
