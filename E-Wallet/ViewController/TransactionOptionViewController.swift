//
//  TransactionOptionViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/4/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class TransactionOptionViewController: UIViewController,CommunicatorProtocol {
    
    @IBOutlet weak var moneyTextField: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
  
    
    func finish(number: String) {
        if let money = moneyTextField.text {
            let refreshAlert = UIAlertController(title: "", message: "هل انت موافق على ارسال مبلغ \(money) الى \(number)", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { (action: UIAlertAction!) in
                self.transaction(number: number)
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "لا", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }

    func transaction(number:String) {
        if let user = Cach.getUser() {
            Utils.showDialog(vc: self)
            RequestAPI.shared.transferMoney(userToken: user.userToken, toPhone: number, amount: moneyTextField.text ?? "0")  { result in
                switch result {
                case .success(_):
                    Utils.dismissDialog()
                    Utils.showAlert(vc: self, title:"نجحت العملية",msg: "تم ارسال المبلغ بنجاح")
                    self.transactionDone()
                    self.moneyTextField.text = ""
                    break
                case let .failure(error):
                    Utils.dismissDialog()
                    Utils.showAlert(vc: self, msg:error)
                    break
                }
            }
        }else {
            Utils.showAlert(vc: self, title:"فشلت العملية",msg: "حدث خطا ما")
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let money = moneyTextField.text , !money.isEmpty {
            showTransactionOption()
        }else {
            Utils.showAlert(vc: self, msg: "برجاء ادخال المبلغ")
        }
    }
    
    func showTransactionOption() {
        let alertController = UIAlertController(title: nil, message: "اختر طريقة الارسال", preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "فحص رمز QR", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.performSegue(withIdentifier: "scan_qr", sender: self)
        })
        
        let action2 = UIAlertAction(title: "رقم الهاتف", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.performSegue(withIdentifier: "search_by_phone", sender: self)
        })
        
        
        let maybeAction = UIAlertAction(title: "الغاء", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Maybe")
        })
        
        let image1 = UIImage(named: "qr-code")
        let image2 = UIImage(named: "phone_number")
        
        
        action1.setValue(image1, forKey: "image")
        action1.setValue(Utils.hexStringToUIColor(hex: "#2fa1ab"), forKey: "titleTextColor")
        action2.setValue(image2, forKey: "image")
        action2.setValue(Utils.hexStringToUIColor(hex: "#2fa1ab"), forKey: "titleTextColor")
        
        maybeAction.setValue(UIColor.red, forKey: "titleTextColor")
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(maybeAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ScanQRViewController {
            let vc = segue.destination as? ScanQRViewController
            vc?.communicator = self
        }else if segue.destination is SearchUserByPhoneViewController {
            let vc = segue.destination as? SearchUserByPhoneViewController
            vc?.communicator = self
        }
    }

    func transactionDone()  {
        if var user = Cach.getUser(),let money = moneyTextField.text {
            let result = Int(user.creditAmount)! - Int(money)!
            user.creditAmount = String(result)
            Cach.saveUser(user: user)
        }
    }
    
    
}
