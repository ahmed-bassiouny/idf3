//
//  LoginViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit
import Firebase
class LoginViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: UITextField!
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        phoneTextField.keyboardType = .asciiCapableNumberPad
        
        self.navigationController?.hideTransparentNavigationBar()
    }
    
    @IBAction func sendCode(_ sender: Any) {
        if phoneTextField.text?.count == 11 {
            getUserInfo()
        }else {
            Utils.showAlert(vc: self, msg: Constant.invalidPhone)
        }
    }
    
    
    
    // MARK: - API Layer
    
    func getUserInfo() {
        if let token = UserDefaults.standard.string(forKey: Constant.TOKEN) , !token.isEmpty{
            Utils.showDialog(vc: self)
            RequestAPI.shared.getUserInfo(phone: phoneTextField.text ?? "",token:token)  { result in
                switch result {
                case let .success(user):
                    self.user = user
                    Utils.dismissDialog()
                    DispatchQueue.main.async {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
                        vc.user = self.user
                        self.present(vc, animated: true, completion: nil)
                    }
                    break
                case let .failure(error):
                    Utils.dismissDialog()
                    Utils.showAlert(vc: self, msg:error)
                    break
                }
            }
        }else {
            Utils.showAlert(vc: self, msg:"برجاء المحاولة فى وقت اخر")
        }
    }
    
    
    
    
}

extension UINavigationController {
    
    public func presentTransparentNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = .clear
        
        
        
        
        setNavigationBarHidden(false, animated:true)
        
        
        
    }
    
    public func hideTransparentNavigationBar() {
        setNavigationBarHidden(true, animated:false)
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
        navigationBar.isTranslucent = UINavigationBar.appearance().isTranslucent
        navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
    }
}
