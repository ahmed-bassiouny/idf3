//
//  AboutViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/13/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit
import WebKit


class AboutViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getAbout()
    }
    
    
    // MARK: - API Layer
    
    func getAbout() {
        Utils.showDialog(vc: self)
        RequestAPI.shared.getAbout(){ result in
            switch result {
            case let .success(Str):
                Utils.dismissDialog()
                self.webView.loadHTMLString(Str, baseURL: nil)
                break
            case let .failure(error):
                Utils.dismissDialog()
                Utils.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    

}
