//
//  SearchUserByPhoneViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/12/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class SearchUserByPhoneViewController: UIViewController {
    
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var userLogo: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var userInfoView: UIView!
    var foundReceiver = false
    var communicator : CommunicatorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        userLogo.layer.masksToBounds = true
    }
    
    
    @IBAction func phoneAction(_ sender: Any) {
        foundReceiver = false
        userInfoView.isHidden = true
        
        if let phone = phoneTextField.text , phone.count == 11 {
            // request user info
            getUserInfo()
        }
        
    }
    
    
    func getUserInfo() {
        Utils.showDialog(vc: self)
        RequestAPI.shared.getUserName(phone: phoneTextField.text ?? "")  { result in
            switch result {
            case let .success(user):
                Utils.dismissDialog()
                self.userNameLabel.text = user.name
                self.userLogo.image = UIImage(named: "placehoulder.png")
                Utils.getImage(image:self.userLogo,url:user.photo)
                self.foundReceiver = true
                self.userLogo.isHidden = false
                self.userNameLabel.isHidden = false
                self.sendBtn.isHidden = true
                self.view.endEditing(true)
                self.userInfoView.isHidden = false
                break
            case .failure(_):
                Utils.dismissDialog()
                self.userNameLabel.text = "لا يوجد هذا الرقم"
                self.userNameLabel.isHidden = false
                self.sendBtn.isHidden = false
                self.foundReceiver = false
                self.userInfoView.isHidden = false
                break
            }
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if foundReceiver {
            self.navigationController?.popViewController(animated: true)
            self.communicator?.finish(number: phoneTextField.text ?? "")
        }
    }
    
    
    @IBAction func sendInvitation(_ sender: UIButton) {
        if let user = Cach.getUser() {
            let str = "\(user.phone) \(Constant.shareTxtInfo) \(Constant.appLink)"
            Utils.shareText(vc: self, text: str)
        }
    }
    
    
}
