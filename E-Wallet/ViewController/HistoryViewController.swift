//
//  HistoryViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/4/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var transaction = [Transaction]()
    
   
    
    
    override func viewDidAppear(_ animated: Bool) {
        errorLabel.isHidden = true
        tableView.isHidden = true
        getHistory()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "history_cell") as! HistoryTableViewCell
        cell.setData(transaction: transaction[indexPath.row])
        return cell
    }
    
    // MARK: - API Layer
    
    func getHistory()  {
        if let user = Cach.getUser() {
            self.loading.startAnimating()
            RequestAPI.shared.getHistory(userToken: user.userToken){
                result in
                switch result {
                case let .success(transaction):
                    self.transaction = transaction
                    self.loading.stopAnimating()
                    if transaction.count == 0 {
                        self.errorLabel.text = "لا يوجد عمليات سابقة"
                    }else {
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                    }
                    break
                case let .failure(error):
                    self.loading.stopAnimating()
                    self.errorLabel.text = error
                    self.errorLabel.isHidden = false
                    break
                }
            }
        }
    }
    
}
