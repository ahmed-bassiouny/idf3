//
//  ScanQRViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/4/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit
import AVFoundation


class ScanQRViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {

    var video = AVCaptureVideoPreviewLayer()
    
    let output = AVCaptureMetadataOutput()
    var communicator : CommunicatorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scanQrCode()
    }
  
    func scanQrCode() {
        
        
        // create session
        let session = AVCaptureSession()
        
        
        // define caputre device
        
        let caputreDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            let input = try AVCaptureDeviceInput(device: caputreDevice!)
            session.addInput(input)
        }catch {
            
        }
        
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        
        // fill the entire screen
        video.frame = view.layer.bounds
        
        view.layer.addSublayer(video)
        
        // view.bringsunbiew(tofront:square)
        
        session.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                if object.type == AVMetadataObject.ObjectType.qr {
                    output.setMetadataObjectsDelegate(nil, queue: DispatchQueue.main)
                    self.navigationController?.popViewController(animated: true)
                    self.communicator?.finish(number: object.stringValue ?? "")
                }
            }
        }
    }

}
