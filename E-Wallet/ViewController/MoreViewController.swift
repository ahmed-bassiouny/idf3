//
//  MoreViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/13/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var listName = ["اشحن الان","العروض","عن ادفع","تعديل الحساب الشخصى","تسجيل الخروج"]
    var listImage = [#imageLiteral(resourceName: "recharge-icon"),#imageLiteral(resourceName: "offers-icon"),#imageLiteral(resourceName: "about"),#imageLiteral(resourceName: "edit"),#imageLiteral(resourceName: "logout")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "more_cell") as! MoreTableViewCell
        cell.moreNameLabel.text = listName[indexPath.row]
        cell.logoImage.image = listImage[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "recharge", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "offer", sender: self)
            break
        case 2:
            performSegue(withIdentifier: "show_about", sender: self)
            break
        case 3:
            performSegue(withIdentifier: "edit_profile", sender: self)
            break
        case 4:
            let alert = UIAlertController(title: "تسجيل الخروج", message: "هل انت متاكد من تسجيل الخروج", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
                // remove cach
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionaryRepresentation()
                dictionary.keys.forEach { key in
                    defaults.removeObject(forKey: key)
                }
                self.navigationController!.viewControllers.removeAll()
                self.dismiss(animated: true, completion: nil)

                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                let navController = UINavigationController(rootViewController: VC1)
                
                self.present(navController, animated:true, completion: nil)

                
            }))
            
             alert.addAction(UIAlertAction(title: "لا", style: UIAlertAction.Style.default, handler: nil))
            
            
            present(alert, animated: true, completion: nil)
            break
        default:
            break
            
        }
    }
    
}
