//
//  CompleteProfileViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class CompleteProfileViewController: UIViewController {
    
    var user:User?
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passcodeTextField: UITextField!
    @IBOutlet weak var confirmPasscode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passcodeTextField.keyboardType = .asciiCapableNumberPad
        confirmPasscode.keyboardType = .asciiCapableNumberPad

        setDataUI()
        // Do any additional setup after loading the view.
    }
    
    func setDataUI()  {
        if let currentUser = user {
            nameTextField.text = currentUser.name
            
        }
    }
    
    @IBAction func comleteButton(_ sender: Any) {
        if nameTextField.text?.isEmpty ?? true {
            Utils.showAlert(vc: self, msg: Constant.invalidName)
        }else if passcodeTextField.text?.count != 4 {
            Utils.showAlert(vc: self, msg: Constant.invalidPasscode)
        }else if passcodeTextField.text != confirmPasscode.text {
            Utils.showAlert(vc: self, msg: Constant.invalidConfirmPasscode)
        }else {
            updateUserName()
        }
    }
    
    // MARK: - updateUserName
    func updateUserName() {
        if let userName = nameTextField.text {
            if let currentUser = user {
                Utils.showDialog(vc: self)
                RequestAPI.shared.updateUser(name: userName, photo: "",userToken: currentUser.userToken) { result in
                    switch result {
                    case let .success(user):
                        self.user = user
                         let passcode = Int(self.passcodeTextField.text ?? "0") ?? 0
                        
                        Cach.saveUser(user: user, passcode:passcode)
                        Utils.dismissDialog()
                        //self.present(PassCodeViewController(), animated: true, completion: nil)
                        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "ValidPassCodeViewController") as! ValidPassCodeViewController
                        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                        self.present(navController, animated:true, completion: nil)
                        //self.performSegue(withIdentifier: "show_home", sender: self)
                        break
                    case let .failure(error):
                        Utils.dismissDialog()
                        Utils.showAlert(vc: self, msg:error)
                        break
                    }
                }
            }
        }
        
    }
}
