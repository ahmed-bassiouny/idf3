//
//  ProfileViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/4/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit
import AVFoundation

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var qrcodeCodeImage: UIImageView!
    
    @IBOutlet weak var imageProfile: UIImageView!
    var qrCode:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageProfile.layer.masksToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setData()
    }
    
    func setData()  {
        
        if let user = Cach.getUser() {
            nameLabel.text = user.name
            phoneLabel.text = user.phone
            Utils.getImage(image:imageProfile,url:user.photo)
            generateQRCode(phone: user.phone)
        }
    }
    
    
    func generateQRCode(phone:String){
        var qrcodeImage: CIImage!
        let data = phone.data(using: .ascii, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        qrcodeImage = (filter?.outputImage)!
        let scaleX = qrcodeCodeImage.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = qrcodeCodeImage.frame.size.height / qrcodeImage.extent.size.height
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        qrCode = convertCIImageToUiImage(transformedImage: transformedImage)
        qrcodeCodeImage.image = qrCode
    }
    
    @IBAction func shareMobileButton(_ sender: Any) {
        if let phone = phoneLabel.text {
            let str = "\(phone) \(Constant.shareTxtInfo) \(Constant.appLink)"
            Utils.shareText(vc: self, text: str)
        }
    }
    
    
    @IBAction func shareQRCode(_ sender: Any) {
        
        
        // let image = qrcodeCodeImage.image
        /*let imageShare = [ image ]
         let activityViewController = UIActivityViewController(activityItems: imageShare , applicationActivities: nil)
         activityViewController.popoverPresentationController?.sourceView = self.view
         self.present(activityViewController, animated: true, completion: nil)
         */
        if let image = qrCode {
            Utils.shareImage(vc: self, image:image)
        }
    }
    
    
    func convertCIImageToUiImage(transformedImage:CIImage?) -> UIImage? {
        if let ciImage = transformedImage {
            let context:CIContext = CIContext.init(options: nil)
            let cgImage:CGImage = context.createCGImage(ciImage, from: ciImage.extent)!
            return UIImage.init(cgImage: cgImage)
        }
        return nil
    }
    
}
