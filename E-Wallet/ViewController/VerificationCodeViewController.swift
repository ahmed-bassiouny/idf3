//
//  VerificationCodeViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/3/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class VerificationCodeViewController: UIViewController {
   
    @IBOutlet weak var firstNumberField: UITextField!
    
    
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        firstNumberField.keyboardType = .asciiCapableNumberPad
         // Do any additional setup after loading the view.
        
        self.navigationController?.presentTransparentNavigationBar()

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is CompleteProfileViewController {
            let vc = segue.destination as? CompleteProfileViewController
            vc?.user = self.user
        }
    }
    
    
    func validCode()  {
        
        let passcode = "" + getValueFromTextfield(firstNumberField)
        
        if user?.verificationCode == passcode {
            performSegue(withIdentifier: "complete_data", sender: self)
        }else {
            Utils.showAlert(vc:self , msg:Constant.invalidPasscodeLogin)
        }
        
    }
    
    func getValueFromTextfield(_ txt:UITextField) -> String {
        return txt.text ?? ""
    }
    
    

    @IBAction func nextButton(_ sender: Any) {
       validCode()
    }
 
    /*func finish() {
        //performSegue(withIdentifier: "showHome", sender: self)
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let viewController = storyboard.instantiateInitialViewController()
        
        if let viewController = viewController {
            self.present(viewController, animated: true, completion: nil)
            
        }
    }*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
