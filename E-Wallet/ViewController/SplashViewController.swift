//
//  SplashViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/5/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        checkUser()
        
        
        /*DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            let alertController = UIAlertController(title: nil, message: "Colored options exists", preferredStyle: .actionSheet)
            
            let action1 = UIAlertAction(title: "Option 1", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Action 1")
            })
            
            let action2 = UIAlertAction(title: "Option 2", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Action 2")
            })
            
            
            let maybeAction = UIAlertAction(title: "Maybe", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
                print("Maybe")
            })
            
            //action1.setValue(UIColor.purple, forKey: "titleTextColor")
            //action2.setValue(UIColor.green, forKey: "titleTextColor")
            //maybeAction.setValue(UIColor.black, forKey: "titleTextColor")
            
            alertController.addAction(action1)
            alertController.addAction(action2)
            alertController.addAction(maybeAction)
            
            self.present(alertController, animated: true, completion: nil)
        })*/
        
    }
   
    func checkUser() {
        if let user = Cach.getUser(), !user.userToken.isEmpty {
//            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//            let navController = UINavigationController(rootViewController: VC1)
//            self.present(navController, animated:true, completion: nil)
//
//            performSegue(withIdentifier: "show_new_user", sender: self)
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: false)
                self.performSegue(withIdentifier: "show_old_user", sender: self)
            }
        }else {
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: false)
                self.performSegue(withIdentifier: "show_new_user", sender: self)
            }

        }
    }
    
}
