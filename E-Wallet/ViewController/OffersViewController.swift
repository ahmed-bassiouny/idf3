//
//  OffersViewController.swift
//  E-Wallet
//
//  Created by NTAM on 3/5/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class OffersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var list = [Offer]()
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var errorLabel: UILabel!
   
    @IBOutlet weak var myTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getOffers()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "offer_cell") as! OfferTableViewCell
        cell.setData(offer: list[indexPath.row])
        return cell
    }

    func getOffers() {
        loading.startAnimating()
        errorLabel.isHidden = true
        myTable.isHidden = true
        RequestAPI.shared.getOffers(){
            result in
            switch result {
            case let .success(offers):
                self.list = offers
                self.loading.stopAnimating()
                if offers.count == 0 {
                    self.errorLabel.text = "لا يوجد عروض حاليا"
                }else {
                    self.myTable.reloadData()
                    self.myTable.isHidden = false
                }
                break
            case let .failure(error):
                self.loading.stopAnimating()
                self.errorLabel.text = error
                self.errorLabel.isHidden = false
                break
            }
        }
    }
}
