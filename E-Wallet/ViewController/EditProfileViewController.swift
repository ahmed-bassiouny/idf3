//
//  EditProfileViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/14/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit
import SDWebImage

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    var photoStr = ""
    var picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        imageProfile.layer.masksToBounds = false
        imageProfile.layer.cornerRadius = imageProfile.frame.height/2
        imageProfile.clipsToBounds = true
        
        picker.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.selectImage))
        self.imageProfile.isUserInteractionEnabled = true
        self.imageProfile.addGestureRecognizer(gesture)
        
        setData()

    }
    
    func setData() {
        // get user
        if let user = Cach.getUser() {
            nameTextField.text = user.name
            imageProfile.sd_setImage(with: URL(string: user.photo), placeholderImage: UIImage(named: "placehoulder.png"))
        }
        
    }
    
    
    @IBAction func save(_ sender: Any) {
    
        
        if nameTextField.text?.isEmpty ?? true {
            Utils.showAlert(vc: self, msg: Constant.invalidName)
        }else {
            if let image = imageProfile.image {
                let imageData:NSData = image.jpeg(.medium)! as NSData
                photoStr = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            updateProfile()
        }
    }
    
    // MARK: - update profile
    
    func updateProfile() {
        if let userName = nameTextField.text {
            if let currentUser = Cach.getUser() {
                Utils.showDialog(vc: self)
                RequestAPI.shared.updateUser(name: userName, photo: photoStr,userToken: currentUser.userToken) { result in
                    switch result {
                    case let .success(user):
                        Cach.saveUser(user: user)
                        Utils.dismissDialog()
                        Utils.showAlert(vc: self,title:Constant.success, msg: Constant.profileUpdated)
                        break
                    case let .failure(error):
                        Utils.dismissDialog()
                        Utils.showAlert(vc: self, msg:error)
                        break
                    }
                }
            }
        }
        
    }
    
    @objc func selectImage() {

        
        let alert = UIAlertController(title: "تغيير الصورة", message: "اختر المصدر", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "الكاميرا", style: .default) { (result : UIAlertAction) -> Void in
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "مكتبة الصور", style: .default) { (result : UIAlertAction) -> Void in
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
        })
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageProfile.image = image
        }
        dismiss(animated: true, completion: nil)
        
        
    }
    
}
