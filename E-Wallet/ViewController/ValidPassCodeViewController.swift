//
//  ValidPassCodeViewController.swift
//  E-Wallet
//
//  Created by NTAM on 2/4/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class ValidPassCodeViewController: UIViewController {
    
    @IBOutlet weak var firstNumberField: UITextField!
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstNumberField.keyboardType = .asciiCapableNumberPad
        // Do any additional setup after loading the view.
        self.addDoneButtonOnKeyboard()
        
    }
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "تم", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.firstNumberField.inputAccessoryView = doneToolbar
        
        
    }
    
    @objc func doneButtonAction()
    {
        validPasscode()
    }
    
    
   
    
    func validPasscode()  {
        let passcodeCach = Cach.getPasscode()
        
        let passcode = Int("" + getValueFromTextfield(firstNumberField))
        
        if passcodeCach == passcode {
            performSegue(withIdentifier: "show_home", sender: self)
        }else {
            Utils.showAlert(vc:self , msg:Constant.invalidPasscodeLogin)
        }
        
    }
    
    func getValueFromTextfield(_ txt:UITextField) -> String {
        return txt.text ?? ""
    }
    
    
}
