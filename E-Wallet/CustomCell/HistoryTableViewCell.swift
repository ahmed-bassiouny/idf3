//
//  HistoryTableViewCell.swift
//  E-Wallet
//
//  Created by NTAM on 2/5/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var imageIcon: UIImageView!
    
    
    var green = Utils.hexStringToUIColor(hex: "#34A93C")
    var red = Utils.hexStringToUIColor(hex: "#F14C4C")

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(transaction:Transaction){
        nameLabel.font = UIFont(name: "font_m", size: nameLabel.font.pointSize)

        nameLabel.text = "الإسم : \(transaction.name)"
        amountLabel.text =  "\(transaction.creditAmount) جنية"
        dateLabel.text = "التاريخ : \(Utils.convertDate(date: transaction.date))"
        
        if transaction.isSender {
            phoneLabel.text = "إرسال الى :  \(transaction.phone)"
            amountLabel.textColor = red
            imageIcon.image = #imageLiteral(resourceName: "Send")
        }else {
            phoneLabel.text = "إستقبال من :  \(transaction.phone)"
            amountLabel.textColor = green
            imageIcon.image = #imageLiteral(resourceName: "receive")
        }
    }
}
