//
//  MoreTableViewCell.swift
//  E-Wallet
//
//  Created by NTAM on 2/13/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {

    
    @IBOutlet weak var moreNameLabel: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
