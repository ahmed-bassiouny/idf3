//
//  OfferTableViewCell.swift
//  E-Wallet
//
//  Created by NTAM on 3/5/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageLogo: UIImageView!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageLogo.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(offer:Offer) {
        priceLabel.text = offer.offer
        nameLabel.text = offer.name
        descLabel.text = offer.description
        Utils.getImage(image:imageLogo,url:offer.logo)
    }

}
